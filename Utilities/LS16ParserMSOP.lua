-- Leishen Main Data Stream Protocol for LS16
leishen_msop_proto = Proto("LeishenMSOP","Leishen Main Data Stream Protocol (MSOP)")

function leishen_msop_proto.dissector(buffer,pinfo,tree)
	pinfo.cols.protocol = "LeishenMSOP"

	local subtree = tree:add(leishen_msop_proto,buffer(),"Leishen Main Data Stream Protocol (MSOP):" ..buffer:len())

	local curr = 0

  -- Check packet size --
	local goodSize = 1206
	local totalSize = buffer:len()
	if totalSize ~= goodSize then return end

	---- Data Blocks ----

	local nbDataBlock = 12
	local sizeDataBlock = 100
	local nbChannel = 32
    local channelSize = 3
	local DataBlockSize = nbDataBlock * nbChannel * channelSize + 4

	local datablocks = subtree:add(buffer(curr, DataBlockSize),"Data blocks")

	for i=1, nbDataBlock
	do
		local dataBlock_subtree = datablocks:add(buffer(curr, sizeDataBlock),"Data Block " ..i)

        -- Flag always 0xff, 0xee
		curr = curr + 2

		local Azimuth = buffer(curr,2):uint()
		dataBlock_subtree:add(buffer(curr,2),"Azimuth  : " .. Azimuth)
		curr = curr + 2

		local lasers = dataBlock_subtree:add(buffer(curr, channelSize * nbChannel),"Channels")

		---- Channels  ----
		for c=0, nbChannel-1
		do
            -- From 1 to 16 first return, 17 to 32 second return data for same channels
			numChannel = c
			local laser_subtree = lasers:add(buffer(curr, 3),"Channel " ..numChannel)

			local Distance = buffer(curr,2):uint()
			laser_subtree:add(buffer(curr,2),"Distance  : " .. Distance)
			curr = curr + 2

			local Reflectivity = buffer(curr,1):uint()
			laser_subtree:add(buffer(curr,1),"Reflectivity  : " .. Reflectivity)
			curr = curr + 1
		end

	end

	-- Footer --

	local footer_subtree = subtree:add(buffer(curr,6),"Footer")

	local TimeStamp = buffer(curr,4):uint()
	footer_subtree:add(buffer(curr,4),"TimeStamp: " .. TimeStamp)
	curr = curr + 4

	local Echo = buffer(curr,1):uint()
	footer_subtree:add(buffer(curr,1),"Echo mode: " .. Echo)
	curr = curr + 1

	local Lidar = buffer(curr,1):uint()
	footer_subtree:add(buffer(curr,1),"Lidar type: " .. Lidar)
	curr = curr + 1
end


-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp port
udp_table:add(2368,leishen_msop_proto)
