# Leishen Plugin

This plugin allow LidarView to be able to read some Leishen LiDARs.

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
