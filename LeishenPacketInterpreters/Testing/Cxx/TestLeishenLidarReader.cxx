/*=========================================================================

  Program: LidarView
  Module:  TestLeishenLidarReader.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TestLeishenInfo.h"

#include "vtkLeishenPacketInterpreter.h"
#include "vtkLidarReader.h"
#include "vtkLidarTestTools.h"

#include <vtkNew.h>
#include <vtkTestUtilities.h>

#include <string>

//----------------------------------------------------------------------------
int TestLeishenLidarReader(int argc, char* argv[])
{
  for (unsigned int idx = 0; idx < TestLeishenInfo::MODELS_NB; idx++)
  {
    const char* modelName = TestLeishenInfo::LIDAR_MODELS[idx];

    std::cout << "\n=========================\n" << std::endl;
    std::cout << "Testing LidarReader for Leishen-" << modelName << " model" << std::endl;

    std::string folderName = vtkTestUtilities::ExpandDataFileName(argc, argv, modelName);
    std::string pcapFileName = folderName + "/" + modelName + ".pcap";
    std::string referenceDataXML = folderName + "/" + modelName + "-reference-data.xml";

    vtkNew<vtkLidarReader> reader;
    vtkNew<vtkLeishenPacketInterpreter> interp;
    interp->SetIgnoreEmptyFrames(true);
    interp->SetIgnoreZeroDistances(true);
    interp->SetEnableAdvancedArrays(true);
    reader->SetLidarInterpreter(interp);
    reader->SetShowPartialFrames(true);
    reader->SetFileName(pcapFileName);

    if (vtkLidarTestTools::TestLidarReaderWithBaseline(reader, referenceDataXML))
    {
      std::cerr << "LidarReader baseline comparaison failed for: " << modelName << " model!"
                << std::endl;
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}
