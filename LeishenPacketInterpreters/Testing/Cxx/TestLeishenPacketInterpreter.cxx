/*=========================================================================

  Program: LidarView
  Module:  TestLeishenPacketInterpreter.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TestLeishenInfo.h"

#include "vtkLeishenPacketInterpreter.h"
#include "vtkLidarReader.h"
#include "vtkLidarTestTools.h"

#include <vtkNew.h>
#include <vtkTestUtilities.h>

#include <string>

//----------------------------------------------------------------------------
int TestLeishenPacketInterpreter(int argc, char* argv[])
{
  for (unsigned int idx = 0; idx < TestLeishenInfo::MODELS_NB; idx++)
  {
    const char* modelName = TestLeishenInfo::LIDAR_MODELS[idx];

    std::cout << "=========================" << std::endl;
    std::cout << "Testing LiDAR interpreter Leishen-" << modelName << " model\n" << std::endl;

    std::string folderName = vtkTestUtilities::ExpandDataFileName(argc, argv, modelName);
    std::string pcapFileName = folderName + "/" + modelName + ".pcap";

    vtkNew<vtkLidarReader> reader;
    vtkNew<vtkLeishenPacketInterpreter> interp;
    interp->SetEnableAdvancedArrays(true);
    reader->SetLidarInterpreter(interp);
    reader->SetFileName(pcapFileName);

    if (vtkLidarTestTools::TestPacketInterpreter(reader))
    {
      std::cerr << "Interpreter test suite failed for: " << modelName << " model!"
                << std::endl;
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}
