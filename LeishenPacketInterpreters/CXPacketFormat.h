/*=========================================================================

  Program:   LidarView
  Module:    ExampleFormat.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef CXPacketFormat_h
#define CXPacketFormat_h

#include <boost/endian/arithmetic.hpp>
#include <boost/predef/other/endian.h>

#include <memory>

#include "InterpreterHelper.h"

namespace leishen_cx
{
constexpr unsigned int PACKET_SIZE = 1206;
constexpr unsigned int MS_PACKET_SIZE = 1212;

constexpr uint8_t DIFOP_ID_HEADER[8] = { 0xA5, 0xFF, 0x00, 0x5A, 0x11, 0x11, 0x55, 0x55 };
constexpr uint8_t DIFOP_ID_TAIL[2] = { 0x0F, 0xF0 };
constexpr uint16_t BLOCK_FLAG = 0xFFEE;
constexpr uint8_t BLOCKS_NB = 12;
constexpr uint8_t CHANNELS_NB = 32;
constexpr uint16_t PACKET_POINTS_NB = leishen_cx::BLOCKS_NB * leishen_cx::CHANNELS_NB;

constexpr uint8_t C16_SIZE = 16;
constexpr uint8_t C32_SIZE = 32;
// clang-format off
constexpr std::array<int16_t, leishen_cx::C16_SIZE> C16_VERTICAL_ANGLES =
  {-16, 0, -14, 2, -12, 4, -10, 6, -8, 8, -6, 10, -4, 12, -2, 14};
constexpr int16_t C32_VERTICAL_ANGLES[leishen_cx::C32_SIZE] =
  { -16, 0, -15, 1, -14, 2, -13, 3, -12, 4, -11, 5, -10, 6, -9, 7,
    -8, 8, -7, 9, -6, 10, -5, 11, -4, 12, -3, 13, -2, 14, -1, 15};
// clang-format on

constexpr double R1_FACTOR = 0.0361;                // in meters
constexpr double CONVERSION_ANGLE = 20.25;          // in degrees
constexpr double DISTANCE_UNIT = 0.0025;            // in meters
constexpr double MS_DISTANCE_UNIT = 0.004;          // in meters
constexpr double C16_CHANNEL_TIME_INTERVAL = 3.125; // in microseconds
constexpr double C16_BLOCK_TIME_INTERVAL = 50.;     // in microseconds
constexpr double C32_CHANNEL_TIME_INTERVAL = 1.536; // in microseconds
constexpr double C32_BLOCK_TIME_INTERVAL = 49.152;  // in microseconds

// Instructs the compiler to pack structure members
#pragma pack(push, 1)

//-----------------------------------------------------------------------------
enum class EchoMode
{
  STRONGEST = 0x37,
  LAST = 0x38,
  DUAL = 0x39
};

//-----------------------------------------------------------------------------
struct ChannelBlock
{
private:
  boost::endian::little_uint16_t Distance;
  boost::endian::little_uint8_t Reflectivity;

public:
  GET_NATIVE_UINT(16, Distance);
  GET_NATIVE_UINT(8, Reflectivity);
};

//-----------------------------------------------------------------------------
struct FiringBlock
{
private:
  boost::endian::big_uint16_t Flag;
  boost::endian::little_uint16_t Azimuth;

public:
  leishen_cx::ChannelBlock Channels[leishen_cx::CHANNELS_NB];

public:
  GET_NATIVE_UINT(16, Flag);
  GET_NATIVE_UINT(16, Azimuth);
};

//-----------------------------------------------------------------------------
struct Footer
{
private:
  boost::endian::little_uint32_t Timestamp;
  boost::endian::little_uint8_t EchoMode;
  boost::endian::little_uint8_t LidarType;

public:
  GET_NATIVE_UINT(32, Timestamp);
  GET_NATIVE_UINT(8, EchoMode);
  GET_NATIVE_UINT(8, LidarType);
};

//-----------------------------------------------------------------------------
class MSOPPacketBase
{
protected:
  struct Data
  {
    leishen_cx::FiringBlock Blocks[leishen_cx::BLOCKS_NB];
    leishen_cx::Footer Footer;
  };

  struct DataMS
  {
    leishen_cx::FiringBlock Blocks[leishen_cx::BLOCKS_NB];
    boost::endian::little_uint8_t UTCTime[6];
    leishen_cx::Footer Footer;
  };

public:
  virtual const leishen_cx::Footer& GetFooter() const = 0;
  virtual const leishen_cx::FiringBlock& GetBlock(uint8_t idx) const = 0;
  /**
   * Get timestamp in microseconds
   */
  virtual double GetTimestamp() const = 0;

  bool IsValid() const
  {
    for (uint8_t idx = 0; idx < leishen_cx::BLOCKS_NB; idx++)
    {
      if (this->GetBlock(idx).GetFlag() != leishen_cx::BLOCK_FLAG)
      {
        return false;
      }
    }
    uint8_t laserNumber = this->GetFooter().GetLidarType();
    if (laserNumber != 16 && laserNumber != 32)
    {
      return false;
    }
    uint8_t echoMode = this->GetFooter().GetEchoMode();
    if (echoMode != 0x37 && echoMode != 0x38 && echoMode != 0x39)
    {
      return false;
    }
    return true;
  }
};

//-----------------------------------------------------------------------------
class MSOPPacket : public MSOPPacketBase
{
private:
  const MSOPPacketBase::Data* Data;

public:
  MSOPPacket(unsigned char const* pkt)
    : Data(reinterpret_cast<const MSOPPacketBase::Data*>(pkt)){};

  const leishen_cx::FiringBlock& GetBlock(uint8_t idx) const override
  {
    return this->Data->Blocks[idx];
  };
  const leishen_cx::Footer& GetFooter() const override { return this->Data->Footer; };
  double GetTimestamp() const override { return this->Data->Footer.GetTimestamp(); };
};

//-----------------------------------------------------------------------------
class MSOPPacketMS : public MSOPPacketBase
{
private:
  const MSOPPacketBase::DataMS* Data;

public:
  MSOPPacketMS(unsigned char const* pkt)
    : Data(reinterpret_cast<const MSOPPacketBase::DataMS*>(pkt)){};

  const leishen_cx::FiringBlock& GetBlock(uint8_t idx) const override
  {
    return this->Data->Blocks[idx];
  };
  const leishen_cx::Footer& GetFooter() const override { return this->Data->Footer; };
  double GetTimestamp() const override
  {
    double secTime = this->Data->UTCTime[2] * 86400 + this->Data->UTCTime[3] * 3600 +
      this->Data->UTCTime[4] * 60 + this->Data->UTCTime[5];
    return secTime * 1e6 + static_cast<double>(this->Data->Footer.GetTimestamp()) * 1e-3;
  };
};

//-----------------------------------------------------------------------------
struct DIFOPPacket
{
private:
  boost::endian::big_uint8_t IdentificationHeader[8];
  boost::endian::big_uint16_t MotorSpeed;
  boost::endian::big_uint8_t EthernetInfo[30];
  boost::endian::big_uint16_t IsLidarStationary;
  boost::endian::big_uint16_t DeviceFlowPacketInterval;
  boost::endian::big_uint8_t Reserved1[4];
  boost::endian::big_uint16_t PPSAlignmentAngleValue;
  boost::endian::big_uint16_t PPSAlignmentDeviationValue;
  boost::endian::big_uint8_t UTCTime[6];
  boost::endian::big_uint8_t LatitudeLongitude[22];
  boost::endian::big_uint8_t Reserved2[106];
  boost::endian::big_uint16_t CX32HorizontalOffset[4];
  boost::endian::big_uint8_t Reserved3[51];
  boost::endian::big_uint16_t CX16VerticalAngle[16];
  boost::endian::big_uint8_t Reserved4[927];
  boost::endian::big_uint8_t FrameTail[2];

public:
  bool IsValid() const
  {
    for (int idx = 0; idx < 8; ++idx)
    {
      if (this->IdentificationHeader[idx] != leishen_cx::DIFOP_ID_HEADER[idx])
      {
        return false;
      }
    }
    for (int idx = 0; idx < 2; ++idx)
    {
      if (this->FrameTail[idx] != leishen_cx::DIFOP_ID_TAIL[idx])
      {
        return false;
      }
    }
    return true;
  };

  GET_NATIVE_UINT(16, MotorSpeed);
  GET_NATIVE_UINT(16, IsLidarStationary);
  GET_NATIVE_UINT(16, PPSAlignmentAngleValue);
  GET_NATIVE_UINT(16, PPSAlignmentDeviationValue);
  uint16_t GetCX32HorizontalOffset(uint8_t spot) const { return this->CX32HorizontalOffset[spot]; };
  int16_t GetCX16VerticalAngle(uint8_t idx) const { return this->CX16VerticalAngle[idx]; };
};

#pragma pack(pop)

};

#endif // CXPacketFormat_h
