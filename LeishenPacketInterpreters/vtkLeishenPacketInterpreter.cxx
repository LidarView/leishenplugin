/*=========================================================================

  Program:   LidarView
  Module:    vtkLeishenPacketInterpreter.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkLeishenPacketInterpreter.h"
#include "InterpreterHelper.h"

#include "CXPacketFormat.h"

#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkShortArray.h>
#include <vtkTypeUInt64Array.h>
#include <vtkUnsignedIntArray.h>

#include <memory>

namespace
{
constexpr const char* CX_MODEL_PREFIX = "CX";
constexpr const char* MSC_MODEL_PREFIX = "MS_C";
constexpr uint16_t MAX_AZIMUTH = 36000;

//-----------------------------------------------------------------------------
std::unique_ptr<const leishen_cx::MSOPPacketBase> InterpretMSOPPacket(unsigned char const* data,
  unsigned int dataLength)
{
  switch (dataLength)
  {
    case leishen_cx::MS_PACKET_SIZE:
      return std::make_unique<const leishen_cx::MSOPPacketMS>(data);

    default:
    case leishen_cx::PACKET_SIZE:
      return std::make_unique<const leishen_cx::MSOPPacket>(data);
  }
}

//-----------------------------------------------------------------------------
bool IsDualEcho(uint8_t echoMode)
{
  return static_cast<leishen_cx::EchoMode>(echoMode) == leishen_cx::EchoMode::DUAL;
}

//-----------------------------------------------------------------------------
int EnsureAzimuthBounds(int azimuth)
{
  return (azimuth + ::MAX_AZIMUTH) % ::MAX_AZIMUTH;
}
}

//-----------------------------------------------------------------------------
class vtkLeishenPacketInterpreter::vtkInternals
{
public:
  //-----------------------------------------------------------------------------
  void ResetAngles()
  {
    std::copy(leishen_cx::C16_VERTICAL_ANGLES.cbegin(),
      leishen_cx::C16_VERTICAL_ANGLES.cend(),
      this->C16VerticalAngles.begin());
    this->C32HorizontalAngleCorrection.fill(0.);
  }

  //-----------------------------------------------------------------------------
  bool IsNewFrame(double currentAzimuth)
  {
    double prevLastAzimuth = this->LastAzimuth;
    this->LastAzimuth = currentAzimuth;
    return currentAzimuth < prevLastAzimuth;
  }

  //-----------------------------------------------------------------------------
  void ApplyCX16VerticalAngleCorrection(const leishen_cx::DIFOPPacket* difopPkt)
  {
    for (int idx = 0; idx < leishen_cx::C16_SIZE; idx++)
    {
      double correctedAngle = difopPkt->GetCX16VerticalAngle(idx) / 100.;
      this->C16VerticalAngles[idx] = correctedAngle;
    }
  }

  //-----------------------------------------------------------------------------
  void ApplyCX32HorizontalAngleCorrection(const leishen_cx::DIFOPPacket* difopPkt)
  {
    for (uint8_t idx = 0; idx < this->C32HorizontalAngleCorrection.size(); idx++)
    {
      double correction = difopPkt->GetCX32HorizontalOffset(idx) / 100.;
      this->C32HorizontalAngleCorrection[idx] = correction;
    }
  }

private:
  double LastAzimuth = 0.;

public:
  vtkSmartPointer<vtkPoints> Points;
  vtkSmartPointer<vtkDoubleArray> PointsX;
  vtkSmartPointer<vtkDoubleArray> PointsY;
  vtkSmartPointer<vtkDoubleArray> PointsZ;
  vtkSmartPointer<vtkShortArray> DualEcho;
  vtkSmartPointer<vtkUnsignedCharArray> Intensity;
  vtkSmartPointer<vtkUnsignedCharArray> LaserId;
  vtkSmartPointer<vtkDoubleArray> Distance;
  vtkSmartPointer<vtkDoubleArray> Timestamp;
  vtkSmartPointer<vtkDoubleArray> Azimuth;

  std::array<double, leishen_cx::C16_SIZE> C16VerticalAngles;
  std::array<double, 4> C32HorizontalAngleCorrection;

  bool IsPreProcessed = false;
  bool IsMSCModel = false;
  unsigned short LaserNumber = 16;
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkLeishenPacketInterpreter)

//-----------------------------------------------------------------------------
vtkLeishenPacketInterpreter::vtkLeishenPacketInterpreter()
  : Internals(new vtkLeishenPacketInterpreter::vtkInternals())
{
  // Theses information will be stored in point cloud field data
  this->SetSensorVendor("Leishen");

  this->ResetCurrentFrame();
}

//-----------------------------------------------------------------------------
vtkLeishenPacketInterpreter::~vtkLeishenPacketInterpreter() = default;

//-----------------------------------------------------------------------------
void vtkLeishenPacketInterpreter::Initialize()
{
  this->Internals->ResetAngles();
  std::string prefix = this->Internals->IsMSCModel ? ::MSC_MODEL_PREFIX : ::CX_MODEL_PREFIX;
  this->SetSensorModelName(prefix + std::to_string(this->Internals->LaserNumber));
  Superclass::Initialize();
}

//-----------------------------------------------------------------------------
void vtkLeishenPacketInterpreter::ProcessPacket(unsigned char const* data, unsigned int dataLength)
{
  auto dataPacket = ::InterpretMSOPPacket(data, dataLength);

  auto& internals = this->Internals;
  double diffAzimuth = 0;

  bool isC16 = dataPacket->GetFooter().GetLidarType() == 16;
  bool isDual = ::IsDualEcho(dataPacket->GetFooter().GetEchoMode());
  for (uint8_t blockId = 0; blockId < leishen_cx::BLOCKS_NB; blockId++)
  {
    if (isDual && this->SkipPointMultipleReturn && blockId % 2 == 1)
    {
      continue;
    }

    const leishen_cx::FiringBlock& currentBlock = dataPacket->GetBlock(blockId);

    if (internals->IsNewFrame(currentBlock.GetAzimuth()))
    {
      // Do not split the frame if the registered points are only the ones of current packet
      // Also if this interpreter is used in pcap reader mode (`IsPreProcessed`) avoid overlapping
      // points, which are before and after the splitting point.
      bool doSplit = internals->Points->GetNumberOfPoints() > leishen_cx::PACKET_POINTS_NB;
      if (doSplit)
      {
        Superclass::SplitFrame();
        if (internals->IsPreProcessed)
        {
          return;
        }
      }
      else if (internals->IsPreProcessed)
      {
        vtkPointData* pointData = this->CurrentFrame->GetPointData();
        for (vtkIdType idx = 0; idx < pointData->GetNumberOfArrays(); idx++)
        {
          pointData->GetAbstractArray(idx)->Reset();
        }
        internals->Points->Reset();
      }
    }

    int nextPacketIdx = isDual ? 2 : 1;
    if (blockId < leishen_cx::BLOCKS_NB - nextPacketIdx)
    {
      int nextAzimuth = dataPacket->GetBlock(blockId + nextPacketIdx).GetAzimuth();
      int currentAzimuth = currentBlock.GetAzimuth();
      diffAzimuth = ::EnsureAzimuthBounds(nextAzimuth - currentAzimuth);
      if (isC16)
      {
        diffAzimuth /= 2;
      }
    }

    for (uint8_t channelId = 0; channelId < leishen_cx::CHANNELS_NB; channelId++)
    {
      bool isC16SecondSet = isC16 && channelId >= leishen_cx::C16_SIZE;
      int currentAzimuth = isC16SecondSet
        ? ::EnsureAzimuthBounds(currentBlock.GetAzimuth() + diffAzimuth)
        : currentBlock.GetAzimuth();
      uint8_t laserId = isC16 ? channelId % leishen_cx::C16_SIZE : channelId;

      // Calculate current horizontal angle
      double horizontalAngle = currentAzimuth / 100.;
      if (isC16)
      {
        horizontalAngle += ((diffAzimuth / 100.) / leishen_cx::C16_SIZE) * laserId;
      }
      else
      {
        horizontalAngle += ((diffAzimuth / 100.) / leishen_cx::C32_SIZE) * laserId;
        // Idx is 0 for channelId ==  [2, 3, 6, 7, ..., 30, 31] and 1 for [0, 1, 4, 5, ..., 28, 29]
        int correctionIdx = (laserId / 2 + 1) % 2;
        horizontalAngle -= internals->C32HorizontalAngleCorrection[correctionIdx];
      }

      // Calculate current vertical angle
      double verticalAngle =
        isC16 ? internals->C16VerticalAngles[laserId] : leishen_cx::C32_VERTICAL_ANGLES[laserId];

      const double distanceUnit =
        internals->IsMSCModel ? leishen_cx::MS_DISTANCE_UNIT : leishen_cx::DISTANCE_UNIT;
      double distance = currentBlock.Channels[channelId].GetDistance() * distanceUnit;
      double intensity = currentBlock.Channels[channelId].GetReflectivity();

      if (distance <= 0.1 || distance > 500.0)
      {
        continue;
      }

      // Calculate point position
      double pos[3];
      horizontalAngle = vtkMath::RadiansFromDegrees(horizontalAngle);
      verticalAngle = vtkMath::RadiansFromDegrees(verticalAngle);
      if (this->InverseXY)
      {
        double laserCorrection =
          vtkMath::RadiansFromDegrees(leishen_cx::CONVERSION_ANGLE - currentAzimuth * 0.01);
        pos[0] = distance * std::cos(verticalAngle) * std::cos(horizontalAngle) +
          leishen_cx::R1_FACTOR * std::cos(laserCorrection);
        pos[1] = distance * std::cos(verticalAngle) * std::sin(horizontalAngle) +
          leishen_cx::R1_FACTOR * std::sin(laserCorrection);
      }
      else
      {
        double laserCorrection =
          vtkMath::RadiansFromDegrees(currentAzimuth * 0.01 - leishen_cx::CONVERSION_ANGLE);
        pos[0] = distance * std::cos(verticalAngle) * std::sin(horizontalAngle) +
          leishen_cx::R1_FACTOR * std::sin(laserCorrection);
        pos[1] = distance * std::cos(verticalAngle) * std::cos(horizontalAngle) +
          leishen_cx::R1_FACTOR * std::cos(laserCorrection);
      }
      pos[2] = distance * std::sin(verticalAngle);

      if (pos[0] == 0. && pos[1] == 0. && pos[2] == 0.)
      {
        continue;
      }

      double pktEndTimestamp = dataPacket->GetTimestamp();

      double blockTI =
        isC16 ? leishen_cx::C16_BLOCK_TIME_INTERVAL * 2. : leishen_cx::C32_BLOCK_TIME_INTERVAL;
      double channelTI =
        isC16 ? leishen_cx::C16_CHANNEL_TIME_INTERVAL : leishen_cx::C32_CHANNEL_TIME_INTERVAL;
      uint8_t invBlockId =
        isDual ? (leishen_cx::BLOCKS_NB - blockId) / 2 : leishen_cx::BLOCKS_NB - blockId;
      double currentTimestamp = pktEndTimestamp - blockTI * invBlockId + channelTI * channelId;

      internals->Points->InsertNextPoint(pos);
      InsertNextValueIfNotNull(internals->PointsX, pos[0]);
      InsertNextValueIfNotNull(internals->PointsY, pos[1]);
      InsertNextValueIfNotNull(internals->PointsZ, pos[2]);
      InsertNextValueIfNotNull(internals->Timestamp, currentTimestamp);
      InsertNextValueIfNotNull(internals->Intensity, intensity);
      InsertNextValueIfNotNull(internals->LaserId, laserId);
      InsertNextValueIfNotNull(internals->Distance, distance);
      InsertNextValueIfNotNull(internals->Azimuth, currentAzimuth);
      InsertNextValueIfNotNull(internals->DualEcho, isDual && blockId % 2);
    }
  }
}

//-----------------------------------------------------------------------------
bool vtkLeishenPacketInterpreter::IsLidarPacket(unsigned char const* data, unsigned int dataLength)
{
  if (dataLength != leishen_cx::PACKET_SIZE && dataLength != leishen_cx::MS_PACKET_SIZE)
  {
    return false;
  }
  auto& internals = this->Internals;

  const leishen_cx::DIFOPPacket* difopPkt = reinterpret_cast<const leishen_cx::DIFOPPacket*>(data);
  if (difopPkt->IsValid())
  {
    if (difopPkt->GetMotorSpeed() > 200)
    {
      this->Rpm = difopPkt->GetMotorSpeed();
    }
    // On our test data, we couldn't determine the vertical angle correction
    // for MS_C16 models
    if (internals->LaserNumber == 16 && !internals->IsMSCModel)
    {
      internals->ApplyCX16VerticalAngleCorrection(difopPkt);
    }
    else if (internals->LaserNumber == 32 && !internals->IsMSCModel)
    {
      internals->ApplyCX32HorizontalAngleCorrection(difopPkt);
    }
    return false;
  }

  auto msopPkt = ::InterpretMSOPPacket(data, dataLength);
  if (msopPkt->IsValid())
  {
    bool isMSCModel = dataLength == leishen_cx::MS_PACKET_SIZE;
    uint8_t laserNumber = msopPkt->GetFooter().GetLidarType();
    if (isMSCModel != internals->IsMSCModel || laserNumber != internals->LaserNumber)
    {
      internals->IsMSCModel = dataLength == leishen_cx::MS_PACKET_SIZE;
      internals->LaserNumber = msopPkt->GetFooter().GetLidarType();
      this->Initialize();
    }
    return true;
  }
  return false;
}

//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> vtkLeishenPacketInterpreter::CreateNewEmptyFrame(vtkIdType nbrOfPoints,
  vtkIdType prereservedNbrOfPoints)
{
  const int defaultPrereservedNbrOfPointsPerFrame = 60000;
  // prereserve for 50% points more than actually received in previous frame
  prereservedNbrOfPoints =
    std::max(static_cast<int>(prereservedNbrOfPoints * 1.5), defaultPrereservedNbrOfPointsPerFrame);

  vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

  // Initialize points
  vtkNew<vtkPoints> points;
  points->SetDataTypeToFloat();
  points->Allocate(prereservedNbrOfPoints);
  if (nbrOfPoints > 0)
  {
    points->SetNumberOfPoints(nbrOfPoints);
  }
  points->GetData()->SetName("Points");
  polyData->SetPoints(points.GetPointer());

  auto& internals = this->Internals;
  internals->Points = points.GetPointer();
  // clang-format off
  InitArrayForPolyData(true, internals->PointsX, "X", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsY, "Y", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsZ, "Z", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(false, internals->Intensity, "intensity", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->LaserId, "laser_id", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Timestamp, "timestamp", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Distance, "distance_m", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Azimuth, "azimuth", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->DualEcho, "dual_return", nbrOfPoints, prereservedNbrOfPoints, polyData);
  // clang-format on

  // Set the default array to display in the application
  polyData->GetPointData()->SetActiveScalars("intensity");
  return polyData;
}

//-----------------------------------------------------------------------------
bool vtkLeishenPacketInterpreter::PreProcessPacket(unsigned char const* data,
  unsigned int dataLength,
  double& outLidarDataTime)
{
  auto& internals = this->Internals;

  internals->IsPreProcessed = true;
  auto dataPacket = ::InterpretMSOPPacket(data, dataLength);
  outLidarDataTime = dataPacket->GetTimestamp() * 1e-6;
  for (uint8_t blockId = 0; blockId < leishen_cx::BLOCKS_NB; blockId++)
  {
    if (internals->IsNewFrame(dataPacket->GetBlock(blockId).GetAzimuth()))
    {
      return true;
    }
  }
  return false;
}
