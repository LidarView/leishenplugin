/*=========================================================================

  Program:   LidarView
  Module:    vtkLeishenPacketInterpreter.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkLeishenPacketInterpreter_h
#define vtkLeishenPacketInterpreter_h

#include <vtkLidarPacketInterpreter.h>

#include <memory>

#include "LeishenPacketInterpretersModule.h"

class LEISHENPACKETINTERPRETERS_EXPORT vtkLeishenPacketInterpreter
  : public vtkLidarPacketInterpreter
{
public:
  static vtkLeishenPacketInterpreter* New();
  vtkTypeMacro(vtkLeishenPacketInterpreter, vtkLidarPacketInterpreter);
  void PrintSelf(ostream& vtkNotUsed(os), vtkIndent vtkNotUsed(indent)) override{};

  /**
   * Initializes the lidar calibration or configuration.
   * This method is called during interpreter initialization, whenever the address/port is changed,
   * or when ResetInitializedState() is called.
   */
  void Initialize() override;

  /**
   * Checks if the current packet is valid for the selected lidar model.
   * Return false when invalid. Invalid packets will be skipped.
   *
   * DIFOP packets (Device Information Output Protocol) will be considered invalid
   * but will be interpreted.
   */
  bool IsLidarPacket(unsigned char const* data, unsigned int dataLength) override;

  /**
   * Builds a frame index for random access when reading a pcap file.
   * Returns true when a new frame is detected.
   *
   * Note that the first packet of the new frame contains info of the previous packet.
   */
  bool PreProcessPacket(unsigned char const* data,
    unsigned int dataLength,
    double& outLidarDataTime) override;

  /**
   * Processes the packet, filling point information using packet data,
   * and calling SplitFrame when necessary.
   */
  void ProcessPacket(unsigned char const* data, unsigned int dataLength) override;

  ///@{
  /**
   * Some algorithms doesn't not support very well multiples return, this flags
   * force skip the multiples return points.
   */
  vtkSetMacro(SkipPointMultipleReturn, bool);
  vtkGetMacro(SkipPointMultipleReturn, bool);
  ///@}

  ///@{
  /**
   * Leishen ROS driver have this option which is reflected here.
   */
  vtkSetMacro(InverseXY, bool);
  vtkGetMacro(InverseXY, bool);
  ///@}

protected:
  /**
   * Creates a new empty frame object, which will be filled by ProcessPacket.
   */
  vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType nbrOfPoints,
    vtkIdType prereservedNbrOfPoints = 60000) override;

  vtkLeishenPacketInterpreter();
  ~vtkLeishenPacketInterpreter();

private:
  vtkLeishenPacketInterpreter(const vtkLeishenPacketInterpreter&) = delete;
  void operator=(const vtkLeishenPacketInterpreter&) = delete;

  bool SkipPointMultipleReturn = false;
  bool InverseXY = false;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif // vtkLeishenPacketInterpreter_h
