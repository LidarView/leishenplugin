set(classes
  vtkLeishenPacketInterpreter
)

set(private_headers
  CXPacketFormat.h
)

vtk_module_add_module(LeishenPlugin::LeishenPacketInterpreters
  CLASSES ${classes}
  PRIVATE_HEADERS ${private_headers}
)

paraview_add_server_manager_xmls(
  MODULE LeishenPlugin::LeishenPacketInterpreters
  XMLS
    LeishenPacketInterpreter.xml
    LeishenLidarReader.xml
    LeishenLidarStream.xml
)
